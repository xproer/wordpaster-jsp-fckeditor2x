﻿var exportwordCommand = function()
{
	//create our own command, we dont want to use the FCKDialogCommand because it uses the default fck layout and not our own
};
exportwordCommand.GetState = function()
{
	return FCK_TRISTATE_OFF; //we dont want the button to be toggled
};
exportwordCommand.Execute = function()
{
	parent.window.zyOffice.api.exportWord();
};

// 注册相关的命令.
FCKCommands.RegisterCommand('exportword', exportwordCommand);

// 创建 "WordPaster" 工具栏按钮.
var plug = new FCKToolbarButton('exportword', FCKLang.exportword);
plug.IconPath = FCKConfig.PluginsPath + 'exportword/z.png';

FCKToolbarItems.RegisterItem('exportword', plug); // 'WordPaster' is the name used in the Toolbar config.
