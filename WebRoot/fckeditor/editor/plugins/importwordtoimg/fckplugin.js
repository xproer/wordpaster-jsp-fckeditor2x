﻿var ImportWordToImgCommand = function()
{
	//create our own command, we dont want to use the FCKDialogCommand because it uses the default fck layout and not our own
};
ImportWordToImgCommand.GetState = function()
{
	return FCK_TRISTATE_OFF; //we dont want the button to be toggled
};
ImportWordToImgCommand.Execute = function()
{
    parent.window.WordPaster.getInstance().importWordToImg();
};

// 注册相关的命令.
FCKCommands.RegisterCommand('importwordtoimg', ImportWordToImgCommand);

// 创建 "WordPaster" 工具栏按钮.
var plugin = new FCKToolbarButton('importwordtoimg', FCKLang.ImportWordToImg);
plugin.IconPath = FCKConfig.PluginsPath + 'importwordtoimg/word1.png';

FCKToolbarItems.RegisterItem('importwordtoimg', plugin); // 'WordPaster' is the name used in the Toolbar config.