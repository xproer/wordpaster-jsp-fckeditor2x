﻿var ExcelImportCommand = function()
{
	//create our own command, we dont want to use the FCKDialogCommand because it uses the default fck layout and not our own
};
ExcelImportCommand.GetState = function()
{
	return FCK_TRISTATE_OFF; //we dont want the button to be toggled
};
ExcelImportCommand.Execute = function()
{
    parent.window.WordPaster.getInstance().importExcel();
};

// 注册相关的命令.
FCKCommands.RegisterCommand('excelimport', ExcelImportCommand);

// 创建 "WordPaster" 工具栏按钮.
var plugin = new FCKToolbarButton('excelimport', FCKLang.ExcelImport);
plugin.IconPath = FCKConfig.PluginsPath + 'excelimport/z.png';

FCKToolbarItems.RegisterItem('excelimport', plugin); // 'WordPaster' is the name used in the Toolbar config.