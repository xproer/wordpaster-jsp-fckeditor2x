﻿var PPTImportCommand = function()
{
	//create our own command, we dont want to use the FCKDialogCommand because it uses the default fck layout and not our own
};
PPTImportCommand.GetState = function()
{
	return FCK_TRISTATE_OFF; //we dont want the button to be toggled
};
PPTImportCommand.Execute = function()
{
    parent.window.WordPaster.getInstance().importPPT();
};

// 注册相关的命令.
FCKCommands.RegisterCommand('pptimport', PPTImportCommand);

// 创建 "WordPaster" 工具栏按钮.
var plugin = new FCKToolbarButton('pptimport', FCKLang.PPTImport);
plugin.IconPath = FCKConfig.PluginsPath + 'pptimport/ppt.png';

FCKToolbarItems.RegisterItem('pptimport', plugin); // 'WordPaster' is the name used in the Toolbar config.