﻿var PPTPasterCommand = function()
{
	//create our own command, we dont want to use the FCKDialogCommand because it uses the default fck layout and not our own
};
PPTPasterCommand.GetState = function()
{
	return FCK_TRISTATE_OFF; //we dont want the button to be toggled
};
PPTPasterCommand.Execute = function()
{
    parent.window.WordPaster.getInstance().PastePPT();
};

// 注册相关的命令.
FCKCommands.RegisterCommand('pptpaster', PPTPasterCommand);

// 创建 "WordPaster" 工具栏按钮.
var plugin = new FCKToolbarButton('pptpaster', FCKLang.PPTPaster);
plugin.IconPath = FCKConfig.PluginsPath + 'pptpaster/ppt.png';

FCKToolbarItems.RegisterItem('pptpaster', plugin); // 'WordPaster' is the name used in the Toolbar config.