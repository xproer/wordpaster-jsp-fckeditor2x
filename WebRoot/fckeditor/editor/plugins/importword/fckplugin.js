﻿var importwordCommand = function()
{
	//create our own command, we dont want to use the FCKDialogCommand because it uses the default fck layout and not our own
};
importwordCommand.GetState = function()
{
	return FCK_TRISTATE_OFF; //we dont want the button to be toggled
};
importwordCommand.Execute = function()
{
	parent.window.zyOffice.api.openDoc();
};

// 注册相关的命令.
FCKCommands.RegisterCommand('importword', importwordCommand);

// 创建 "WordPaster" 工具栏按钮.
var plug = new FCKToolbarButton('importword', FCKLang.importword);
plug.IconPath = FCKConfig.PluginsPath + 'importword/z.png';

FCKToolbarItems.RegisterItem('importword', plug); // 'WordPaster' is the name used in the Toolbar config.
