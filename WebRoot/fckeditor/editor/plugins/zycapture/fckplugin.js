﻿var zyCaptureCommand = function()
{
	//create our own command, we dont want to use the FCKDialogCommand because it uses the default fck layout and not our own
};
zyCaptureCommand.GetState = function()
{
	return FCK_TRISTATE_OFF; //we dont want the button to be toggled
};
zyCaptureCommand.Execute = function()
{
	parent.window.zyCapture.Capture2();
};

// 注册相关的命令.
FCKCommands.RegisterCommand('zycapture', zyCaptureCommand);

// 创建 "WordPaster" 工具栏按钮.
var plug = new FCKToolbarButton('zycapture', FCKLang.ScreenCapture);
plug.IconPath = FCKConfig.PluginsPath + 'zycapture/z.png';

FCKToolbarItems.RegisterItem('zycapture', plug); // 'WordPaster' is the name used in the Toolbar config.
