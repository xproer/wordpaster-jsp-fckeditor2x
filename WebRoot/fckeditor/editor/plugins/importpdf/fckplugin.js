﻿var importpdfCommand = function()
{
	//create our own command, we dont want to use the FCKDialogCommand because it uses the default fck layout and not our own
};
importpdfCommand.GetState = function()
{
	return FCK_TRISTATE_OFF; //we dont want the button to be toggled
};
importpdfCommand.Execute = function()
{
	parent.window.zyOffice.api.openPdf();
};

// 注册相关的命令.
FCKCommands.RegisterCommand('importpdf', importpdfCommand);

// 创建 "WordPaster" 工具栏按钮.
var plug = new FCKToolbarButton('importpdf', FCKLang.importpdf);
plug.IconPath = FCKConfig.PluginsPath + 'importpdf/z.png';

FCKToolbarItems.RegisterItem('importpdf', plug); // 'WordPaster' is the name used in the Toolbar config.
