﻿var WordImportCommand = function()
{
	//create our own command, we dont want to use the FCKDialogCommand because it uses the default fck layout and not our own
};
WordImportCommand.GetState = function()
{
	return FCK_TRISTATE_OFF; //we dont want the button to be toggled
};
WordImportCommand.Execute = function()
{
    parent.window.WordPaster.getInstance().importWord();
};

// 注册相关的命令.
FCKCommands.RegisterCommand('wordimport', WordImportCommand);

// 创建 "WordPaster" 工具栏按钮.
var plugin = new FCKToolbarButton('wordimport', FCKLang.WordImport);
plugin.IconPath = FCKConfig.PluginsPath + 'wordimport/z.png';

FCKToolbarItems.RegisterItem('wordimport', plugin); // 'WordPaster' is the name used in the Toolbar config.