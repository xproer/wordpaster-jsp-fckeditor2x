﻿var PDFImportCommand = function()
{
	//create our own command, we dont want to use the FCKDialogCommand because it uses the default fck layout and not our own
};
PDFImportCommand.GetState = function()
{
	return FCK_TRISTATE_OFF; //we dont want the button to be toggled
};
PDFImportCommand.Execute = function()
{
    parent.window.WordPaster.getInstance().ImportPDF();
};

// 注册相关的命令.
FCKCommands.RegisterCommand('pdfimport', PDFImportCommand);

// 创建 "WordPaster" 工具栏按钮.
var plugin = new FCKToolbarButton('pdfimport', FCKLang.PDFImport);
plugin.IconPath = FCKConfig.PluginsPath + 'pdfimport/pdf.png';

FCKToolbarItems.RegisterItem('pdfimport', plugin); // 'WordPaster' is the name used in the Toolbar config.